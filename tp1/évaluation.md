# Évaluation de votre travail pratique 1

**Note totale**: 0/100

## Identification

- Cours      : Utilisation et administration des systèmes informatiques
- Sigle      : INF1070
- Session    : Hiver 2019
- Groupe     : `<numéro du groupe>`
- Enseignant : `<nom de votre enseignant>`
- Auteur     : `<votre nom>` (`<votre code permanent>`)
- Auteur     : `<votre nom>` (`<votre code permanent>`)

## Présentation globale

**Pénalité**: 0 pts

### Commentaires

Rien à signaler

## Mission 2

**Note**: 0/5

### Commentaires

Rien à signaler.

## Mission 3

**Note**: 0/5

### Commentaires

Rien à signaler.

## Mission 4

**Note**: 0/5

### Commentaires

Rien à signaler.

## Mission 5

**Note**: 0/5

### Commentaires

Rien à signaler.

## Mission 6

**Note**: 0/5

### Commentaires

Rien à signaler.

## Mission 7

**Note**: 0/5

### Commentaires

Rien à signaler.

## Mission 8

**Note**: 0/5

### Commentaires

Rien à signaler.

## Mission 9

**Note**: 0/5

### Commentaires

Rien à signaler.

## Mission 10

**Note**: 0/6

### Commentaires

Rien à signaler.

## Mission 11

**Note**: 0/6

### Commentaires

Rien à signaler.

## Mission 12

**Note**: 0/6

### Commentaires

Rien à signaler.

## Mission 13

**Note**: 0/6

### Commentaires

Rien à signaler.

## Mission 14

**Note**: 0/6

### Commentaires

Rien à signaler.

## Mission 15

**Note**: 0/6

### Commentaires

Rien à signaler.

## Mission 16

**Note**: 0/6

### Commentaires

Rien à signaler.

## Mission 17

**Note**: 0/6

### Commentaires

Rien à signaler.

## Mission 18

**Note**: 0/6

### Commentaires

Rien à signaler.

## Mission 19

**Note**: 0/6

### Commentaires

Rien à signaler.

<!-- vim: set sts=2 ts=2 sw=2 tw=80 et :-->

