# Évaluation du travail pratique 2

Note totale: ??/100

## Identification

- Cours      : Utilisation et administration des systèmes informatiques
- Sigle      : INF1070
- Session    : Hiver 2019
- Groupe     : `<numéro du groupe>`
- Enseignant : `<nom de votre enseignant>`
- Auteur     : `<votre nom>` (`<votre code permanent>`)
- Auteur     : `<votre nom>` (`<votre code permanent>`)

## Qualité de la remise: ??/20

Commentaires s'il y a lieu

## Exercice 1: ??/8

Commentaires s'il y a lieu

## Exercice 2: ??/8

Commentaires s'il y a lieu

## Exercice 3: ??/8

Commentaires s'il y a lieu

## Exercice 4: ??/8

Commentaires s'il y a lieu

## Exercice 5: ??/8

Commentaires s'il y a lieu

## Exercice 6: ??/8

Commentaires s'il y a lieu

## Exercice 7: ??/8

Commentaires s'il y a lieu

## Exercice 8: ??/8

Commentaires s'il y a lieu

## Exercice 9: ??/8

Commentaires s'il y a lieu

## Exercice 10: ??/8

Commentaires s'il y a lieu

<!-- vim: set sts=2 ts=2 sw=2 tw=80 et :-->

