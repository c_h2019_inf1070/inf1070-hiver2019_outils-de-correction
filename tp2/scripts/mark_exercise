#!/bin/bash

smark=

get_interpreter() {
  local scriptname="$1"
  grep -oP '^\s*#!\s*/bin/\K[a-z]+\s*$' "$scriptname"
}

mark_question() {
  printf "($1) ${YELLOW}${BOLD}Quelle note attribuer (??/$2) ?${NO_COLOUR} "
  read smark

  while ! grep -qE "^[0-9]+$" <<< $smark || (( $smark > $2 )); do
    printf "Entrer un nombre entre 0 et $2. " >&2
    read smark
  done
}
attribute_mark() { sed -i "/$1\\s*:\\s\\+.*\\/$3/s/:.*$/: $2\/$3/" "$eval_file"; }
already_marked() { grep -E "$1\\s*:\\s+(10|[0-9])\\/$2" "$eval_file"; }
remark() {
  if already_marked "$1" "$2"; then
    print_yesno_question "La note est déjà attribuée. Refaire ?"
    read on
    [[ $on =~ ou?i? ]] && return 0 || return 1
  fi
}

setup_test_dir() {
  td=$(mktemp -d)
  local scriptname=$1 ; shift
  local tp2_exercise_dir="$tp2_dir/$(basename "$PWD")"

  local testscriptdest=$td/$scriptname.test
  local solscriptdest=$td/$scriptname.sol
  cp "$solutions_dir/$scriptname" $solscriptdest
  testscriptsrcapprox=$(sed 's/[aeiou]/?/g' <<< "*${scriptname%.*}*")
  read testscriptsrc < <(find -type f -iname "$testscriptsrcapprox" -not -name ".*")
  if ! [[ $testscriptsrc ]]; then
    print_error "Fichier introuvable"
    return 1
  fi
  # Déplacement du script de l'étudiant en le convertissant à UTF-8
  # Autrement, y'a des bugs de caractères avec `sed` / `grep`.
  if file -i "$testscriptsrc" | grep -qioP 'charset=\K.*utf.*?;?' ; then
    install -m 755 "$testscriptsrc" $testscriptdest
  else
    iconv -c -f LATIN1 -t UTF8 "$testscriptsrc" >$testscriptdest
    chmod 755 $testscriptdest
  fi
  dos2unix $testscriptdest
  [[ "$@" != "" ]] && cp -rt $td "$@"
  [[ $docker_cid ]] && docker cp $td $docker_cid:$td
  echo $td
}

# Récupère les fichiers générés dans l'environnement Docker.
docker_pull_testdir() { [[ $docker_cid ]] && docker cp $docker_cid:$td "${td%/*}"; }
# Exécute une commande dans Docker.
docker_run_this()     {
  docker exec -w "$td" $docker_cid bash -c "$*"
  rc=$?
  docker_pull_testdir
  return $rc
}
# Exécute une commande dans Docker ou directement dans `bash` dépendemment du
# cas (valeur de `docker_cid` non vide si `-d` a été passé au script).
run_this()            { if [[ $docker_cid ]]; then docker_run_this "$*" ; else bash -c "$*"; fi ; }
run_this_script()     {
  local scriptname="$1"
  local shell_=bash
  check_bad_shebang "$scriptname" &>/dev/null || shell_=$(get_interpreter "$scriptname")
  run_this "$shell_ $*" ;
}
# Exécute une liste de commandes séparées sur des lignes distinctes. Une virgule
# délimite celle-ci avec un second argument faisant office de commentaire
# affiché entre parenthèses.
run_these() {
  while IFS=, read args com; do
    [[ $com ]] && print_step "$args ($com)" || print_step "$args"
    run_this_script $args </dev/null
  done
}
docker_clean()        { docker exec $docker_cid rm -rf "$td";          }
clean()               { [[ $docker_cid ]] && docker_clean; rm -rf $td; }
clean_and_exit()      { clean ; echo ; exit 255;                       }

check_bad_shebang() {
  local file=$1
  grep -q '#!\s*/bin/sh' $file \
    && grep -qP '\[\[.*?\]\]|((|<<<|<\(\)|<\(.*?\)|\<let\>' $file \
    && print_warning_blink "Shebang incohérent!"
}

test_hex2c() {
  local scriptname=hex2c.sh
  local test_files="$mark_template_dir/fichiers-de-test/hex2c/*"
  td=$(setup_test_dir $scriptname $test_files) || return 128
  cd $td
  local number_of_datafiles=5

  # test des cas d'erreurs
  print_action "Cas d'erreur"
  r=$RANDOM
  run_these <<EOF
./$scriptname.test,sans argument
./$scriptname.test $r,fichier inexistant
EOF

  # test des cas fonctionnels
  print_action "Cas fonctionnels"
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    for (( i = 1; i <= ${number_of_datafiles}; i++ )); do
      run_these <<EOF
./$scriptname.$s ${i}_hex >$s.out.$i.hex
./$scriptname.$s - < ${i}_hex >$s.out.$i.hex.stdin
./$scriptname.$s -h ${i}_hexdump >$s.out.$i.hexdump
./$scriptname.$s -h - < ${i}_hexdump >$s.out.$i.hexdump.stdin
EOF
    done
  done
  print_diff_display $scriptname
  for (( i = 1; i <= ${number_of_datafiles}; i++ )); do
    d=$(diff <(xxd test.out.$i.hex) <(xxd sol.out.$i.hex)) \
      || echo -e "${YELLOW}Différences entre test.out.$i.hex (<) et sol.out.$i.hex (>)${NO_COLOUR}:\
                 \n$d\n" | colordiff
    d=$(diff <(xxd test.out.$i.hex.stdin) <(xxd sol.out.$i.hex.stdin)) \
      || echo -e "${YELLOW}Différences entre test.out.$i.hex.stdin (<) et sol.out.$i.hex.stdin (>)${NO_COLOUR}:\
                 \n$d\n" | colordiff
    d=$(diff <(xxd test.out.$i.hexdump) <(xxd sol.out.$i.hexdump)) \
      || echo -e "${YELLOW}Différences entre test.out.$i.hexdump (<) et sol.out.$i.hexdump (>)${NO_COLOUR}:\
                 \n$d\n" | colordiff
    d=$(diff <(xxd test.out.$i.hexdump.stdin) <(xxd sol.out.$i.hexdump.stdin)) \
      || echo -e "${YELLOW}Différences entre test.out.$i.hexdump.stdin (<) et sol.out.$i.hexdump.stdin (>)${NO_COLOUR}:\
                 \n$d\n" | colordiff
  done
  check_bad_shebang $scriptname.test
  return 0
}

test_metarkane() {
  local scriptname=metarkane.sh
  td=$(setup_test_dir $scriptname) || return 128
  cd $td

  local wordsource=/usr/share/dict/french
  local outdir=sortie
  local words=('Saluton' Kiel vi 'estas')

  # test des cas d'erreurs
  print_action "Cas d'erreur"
  run_these <<EOF
./$scriptname.test,sans argument
./$scriptname.test $wordsource 3,2 arguments avec 4 min
./$scriptname.test $RANDOM 3 $outdir toto,fichier inexistant
./$scriptname.test $wordsource tutu $outdir toto,taille pas un nombre
EOF

  # test des cas fonctionnels
  print_action "Cas fonctionnels"
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    for i in $(seq 5); do
      args="./$scriptname.$s $wordsource $i $outdir.$s.$i ${words[@]}"
      print_step "$args"
      run_this_script $args
      run_this "find $outdir.$s.$i -type f | sort -k2 -t/ | xargs tail -qc $i | sed -E '\$s/(.*)/\\1\\n/' >$s.out.$i"
    done
    print_action "Affichage de l'arborescence (pour longueur=5)"
    tree $outdir.$s.5
  done
  print_diff_display $scriptname
  for i in $(seq 5); do
    d=$(diff test.out.$i sol.out.$i) \
      || echo -e "${YELLOW}Différences entre test.out.$i (<) et sol.out.$i (>)${NO_COLOUR}:\
                 \n$d\n" | colordiff
  done
  check_bad_shebang $scriptname.test
  return 0
}

test_natcalc() {
  local scriptname=natcalc.sh
  local test_files="$mark_template_dir/fichiers-de-test/natcalc/*"
  td=$(setup_test_dir $scriptname $test_files) || return 128
  cd $td

  local number_of_datafiles=4

  # test des cas d'erreurs
  print_action "Cas d'erreur"
  r=$RANDOM
  run_these <<EOF
./$scriptname.test,sans argument
./$scriptname.test $r,fichier inexistant
EOF

  # test des cas fonctionnels
  print_action "Cas fonctionnels"
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    for i in $(seq $number_of_datafiles); do
      args="./$scriptname.$s in.$i >>$s.out.$i"
      print_step "$args"
      run_this_script $args
    done
  done

  print_diff_display $scriptname
  for i in $(seq $number_of_datafiles); do
    d=$(diff test.out.$i sol.out.$i) \
      || echo -e "${YELLOW}Différences entre test.out.$i (<) et sol.out.$i (>)${NO_COLOUR}:\
                 \n$d\n" | colordiff
  done
  check_bad_shebang $scriptname.test
  return 0
}

test_nobel() {
  local scriptname=nobel.sh
  td=$(setup_test_dir $scriptname) || return 128
  cd $td

  local fields=(physics economics medicine peace)
  local years=( \
    $((($RANDOM%118)+1901)) \
    $((($RANDOM%118)+1901)) \
    $((($RANDOM%118)+1901)) \
    $((($RANDOM%118)+1901)))


  # test des cas d'erreurs
  print_action "Cas d'erreur"
  run_these <<EOF
./$scriptname.test,sans arguments
./$scriptname.test 1996,juste l'année
./$scriptname.test physics,juste le champ d'étude
./$scriptname.test physics 1996,arguments inversés
EOF

  # test des cas fonctionnels
  print_action "Cas fonctionnels"
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    for i in $(seq 0 3); do
      args="./$scriptname.$s ${years[$i]} ${fields[$i]} >$s.out.${fields[$i]}"
      print_step "$args"
      run_this_script $args
    done
  done

  print_diff_display $scriptname
  for f in "${fields[@]}" ; do
    d=$(diff test.out.$f sol.out.$f) \
      || echo -e "${YELLOW}Différences entre test.out.$f (<) et sol.out.$f (>)${NO_COLOUR}:\
                 \n$d\n" | colordiff
  done
  check_bad_shebang $scriptname.test
  return 0
}

test_boule() {
  local scriptname=boule
  td=$(setup_test_dir $scriptname) || return 128
  cd $td

  # test des cas d'erreurs
  print_action "Cas d'erreur"
  run_these <<EOF
./$scriptname.test,sans arguments
EOF

  # test des cas fonctionnels
  print_action "Cas fonctionnels"
  print_exec_script $scriptname
  run_these <<EOF
./$scriptname.test Suis-je beau?
./$scriptname.test Mon gazon est-il plus beau que celui de mon voisin?
./$scriptname.test Ĉu vi parolas la esperantan?
EOF
check_bad_shebang $scriptname.test
return 0
}

test_statistique() {
  local scriptname=statistique
  local test_files="$mark_template_dir/fichiers-de-test/statistique/*"
  td=$(setup_test_dir $scriptname $test_files) || return 128
  cd $td

  local dirs=(titi toto)

  # test des cas d'erreurs
  print_action "Cas d'erreur"
  run_these <<EOF
./$scriptname.test bidon.txt,Fichier existant
./$scriptname.test $RANDOM,Répertoire inexistant
EOF

  # test des cas fonctionnels
  print_action "Cas fonctionnels"
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    for dir in "${dirs[@]}" ; do
      args="./$scriptname.$s $dir | sed 's,/$,,' >$s.out.$dir"
      print_step "$args"
      run_this_script $args
      grep -iPo 'nombre de (r[ée]p[eé]rtoires?|dossiers?)\s*:\s*\K[0-9]+' $s.out.$dir >$s.reps.$dir
      grep -iPo 'nombre de r[ée]p[eé]rtoires? vides?\s*:\s*\K[0-9]+' $s.out.$dir >$s.repsv.$dir
      grep -iPo 'nombre de fichiers?\s*:\s*\K[0-9]+' $s.out.$dir >$s.fs.$dir
      grep -iPo 'nombre de liens? symboliques?\s*:\s*\K[0-9]+' $s.out.$dir >$s.ls.$dir
      grep -iPo 'taille\s*:\s*\K[0-9]+.*' $s.out.$dir >$s.taille.$dir
    done
  done
  print_action "Affichage d'un échantillon"
  cat "test.out.${dirs[0]}"

  print_diff_display $scriptname
  for dir in "${dirs[@]}" ; do
    d=$(diff test.reps.$dir sol.reps.$dir) \
      || echo -e "${YELLOW}Différences du nombre de répertoires\
 sous $dir entre test (<) et sol (>)${NO_COLOUR}:\n$d\n" | colordiff
    d=$(diff test.repsv.$dir sol.repsv.$dir) \
      || echo -e "${YELLOW}Différences du nombre de répertoires vides\
 sous $dir entre test (<) et sol (>)${NO_COLOUR}:\n$d\n" | colordiff
    d=$(diff test.fs.$dir sol.fs.$dir) \
      || echo -e "${YELLOW}Différences du nombre de fichiers\
 sous $dir entre test (<) et sol (>)${NO_COLOUR}:\n$d\n" | colordiff
    d=$(diff test.ls.$dir sol.ls.$dir) \
      || echo -e "${YELLOW}Différences du nombre liens symboliques\
 sous $dir entre test (<) et sol (>)${NO_COLOUR}:\n$d\n" | colordiff
    d=$(diff test.taille.$dir sol.taille.$dir) \
      || echo -e "${YELLOW}Différences sur la taille de $dir\
 entre test (<) et sol (>)${NO_COLOUR}:\n$d\n" | colordiff
  done
  check_bad_shebang $scriptname.test
  return 0
}

test_todo() {
  local scriptname=todo
  td=$(setup_test_dir $scriptname) || return 128
  cd $td

  tasks=$(echo -ne \
    "Konigi mian sciojn\n\
    Lernigi la esperantan al ĉiuj\n\
    Endormiĝi ekde la 22an\n"
  )
  taskscount=$(wc -l <<< "$tasks")
  PREF="export HOME=$td ;"

  # test des cas d'erreurs
  print_action "Cas d'erreur"
  run_these <<EOF
./$scriptname.test afficher,Aucune liste de tâches
./$scriptname.test supprimer,Aucune liste de tâches
./$scriptname.test ajouter,Aucun argument
./$scriptname.test retirer,Mauvais nom de fonction
./$scriptname.test supprimer toto,No de tâche pas un nombre
./$scriptname.test supprimer 5,No de tâche trop grand
EOF

  # test des cas fonctionnels
  print_action "Cas fonctionnels"
  for s in test sol ; do
    shell_=$(get_interpreter $scriptname.$s)

    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    while read t ; do
      args="${PREF} $shell_ ./$scriptname.$s ajouter \"$t\""
      print_step "$args"
      run_this "$args"
    done <<< "$tasks"
    run_this "${PREF} cp ~/todo_liste ~/todo_liste.pleine.$s"
    print_action "Suppression des tâches 1, 1, ..."
    while read _; do
      args="${PREF} $shell_ ./$scriptname.$s supprimer 1"
      print_step "$args"
      run_this "$args"
    done <<< "$tasks"
    if run_this "${PREF} test -f ~/todo_liste"; then
      print_error "~/todo_liste pas supprimé"
      print_action "Affichage du contenu"
      run_this "${PREF} cat ~/todo_liste"
    fi
    print_action "Restoration et suppression des tâches 5,4,..."
    run_this "${PREF} cp ~/todo_liste.pleine.$s ~/todo_liste"
    for (( i = $taskscount; i > 0 ; i-- )); do
      args="${PREF} $shell_ ./$scriptname.$s supprimer $i"
      print_step "$args"
      run_this "$args"
    done
    mtn=$(($taskscount/2+1))
    print_action "Restoration et suppression de la tâche $mtn"
    run_this "${PREF} cp ~/todo_liste.pleine.$s ~/todo_liste"
    run_this "${PREF} $shell_ ./$scriptname.$s supprimer $mtn"
    print_action "Sauvegarde de la liste de tâche pour $s"
    run_this "${PREF} mv ~/todo_liste ~/todo_liste.$s"
  done

  print_diff_display $scriptname
  d=$(diff todo_liste.pleine.test todo_liste.pleine.sol) \
    || echo -e "${YELLOW}Différences entre todo_liste.pleine.test (<) et todo_liste.pleine.sol (>)\
               ${NO_COLOUR}:\n$d\n" | colordiff
  d=$(diff todo_liste.test todo_liste.sol) \
    || echo -e "${YELLOW}Différences entre todo_liste.test (<) et todo_liste.sol (>)${NO_COLOUR}:\
               \n$d\n" | colordiff
  check_bad_shebang $scriptname.test
  return 0
}

test_miniprompt() {
  local scriptname=miniprompt.sh
  td=$(setup_test_dir $scriptname) || return 128
  cd $td
  diff $scriptname.test $scriptname.sol | colordiff
  check_bad_shebang $scriptname.test
  return 0
}

test_minitac() {
  local scriptname=minitac.sh
  local test_files="$mark_template_dir/fichiers-de-test/minitac/*"
  td=$(setup_test_dir $scriptname $test_files) || return 128
  cd $td

  grep -q '\<tac\>' $scriptname.test \
    && print_warning_blink "Le programme \`tac\` est peut-être utilisé. Investiguer."

  testfs=(1a10 blub)

  # test des cas d'erreurs
  print_action "Cas d'erreur"
  run_these <<EOF
./$scriptname.test,sans arguments
./$scriptname.test 1a10,un seul argument
./$scriptname.test a$RANDOM b$RANDOM,fichier inexistant
./$scriptname.test <(echo tutu; echo tata),fichier non régulier
EOF

  # test des cas fonctionnels
  print_action "Cas fonctionnels"
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    for f in "${testfs[@]}" ; do
      args="./$scriptname.$s $f $s.out.$f"
      print_step "$args"
      run_this_script $args
    done
  done

  print_diff_display $scriptname
  for f in "${testfs[@]}" ; do
    d=$(diff test.out.$f sol.out.$f) \
      || echo -e "${YELLOW}Différences entre test.out.$f (<) et sol.out.$f (>)${NO_COLOUR}:\
                 \n$d\n" | colordiff
  done
  check_bad_shebang $scriptname.test
  return 0
}

test_minitop() {
  local scriptname=minitop.sh
  td=$(setup_test_dir $scriptname) || return 128
  cd $td

  # test des cas d'erreurs
  print_action "Cas d'erreur"
  run_these <<EOF
./$scriptname.test 30,Trop grand paramètre
EOF

  # test des cas fonctionnels
  print_action "Cas fonctionnels"
  for s in test sol ; do
    [[ $s == test ]] && print_exec_script $scriptname || print_exec_sol $scriptname
    args="./$scriptname.$s 0 >$s.out.0"
    print_step "$args"
    run_this_script $args
  done

  print_diff_display $scriptname
  d=$(diff test.out.0 sol.out.0) \
    || echo -e "${YELLOW}Différences entre test.out.0 (<) et sol.out.0 (>)${NO_COLOUR}:\n$d" \
    | colordiff
  print_action "Affichage en boucle (CTRL+C pour arrêter)"
  args="./$scriptname.test"
  print_step "$args"
  trap : SIGINT
  run_this_script $args
  trap clean_and_exit SIGINT
  check_bad_shebang $scriptname.test
  return 0
}

mark_exercise() {
  trap clean_and_exit SIGINT
  local exercise="$1"
  local total_pts=8
  local rc=
  if ! cd $exercise 2>/dev/null; then
    print_error "Format des répertoires non-respectés."
    print_warning "Tentative de trouver le bon répertoire."
    exercisedirpath=$(sed 's/[aeiou]/?/g' <<< "*${exercise}*")
    read exercisedir < <(find -maxdepth 1 -type d -iname "$exercisedirpath")
    if ! [[ $exercisedir ]] || ! cd "$exercisedir" 2>/dev/null; then
      print_error "Impossible de trouver le répertoire."
      print_warning "Tentative d'utiliser le répertoire courant."
    fi
  fi
  case "$exercise" in
    hex2c)
      local exercise_text="Exercice 1"
      remark "$exercise_text" $total_pts || return
      test_hex2c
      ;;
    metarkane)
      local exercise_text="Exercice 2"
      remark "$exercise_text" $total_pts || return
      test_metarkane
      ;;
    natcalc)
      local exercise_text="Exercice 3"
      remark "$exercise_text" $total_pts || return
      test_natcalc
      ;;
    nobel)
      local exercise_text="Exercice 4"
      remark "$exercise_text" $total_pts || return
      test_nobel
      ;;
    boule)
      local exercise_text="Exercice 5"
      remark "$exercise_text" $total_pts || return
      test_boule
      ;;
    statistique)
      local exercise_text="Exercice 6"
      remark "$exercise_text" $total_pts || return
      test_statistique
      ;;
    todo)
      local exercise_text="Exercice 7"
      remark "$exercise_text" $total_pts || return
      test_todo
      ;;
    miniPrompt)
      local exercise_text="Exercice 8"
      remark "$exercise_text" $total_pts || return
      test_miniprompt
      ;;
    miniTac)
      local exercise_text="Exercice 9"
      remark "$exercise_text" $total_pts || return
      test_minitac
      ;;
    miniTop)
      local exercise_text="Exercice 10"
      remark "$exercise_text" $total_pts || return
      test_minitop
      ;;
  esac
  rc=$?
  if (( $rc == 128 )); then
    print_warning "Note 0 attribuée."
    attribute_mark "$exercise_text" 0 $total_pts
  else
    continue_or_shell . "Répertoire de test" "$docker_cid"
    mark_question "$exercise" $total_pts
    attribute_mark "$exercise_text" $smark $total_pts
  fi
  clean $td # Nettoyage des fichiers temporaires
  return $rc
}

# vim: set sts=2 ts=2 sw=2 tw=120 et :

