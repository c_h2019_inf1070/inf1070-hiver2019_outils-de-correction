#!/bin/sh

API_PATH="http://api.nobelprize.org/v1/prize.json"

usage()
{
	echo "$0 <annee> <categorie>"
	echo "Récupère les lauréats d'un prix Nobel et filtre en fonction des critères de recherche."
}

if [ $# -lt 2 ]; then
	usage >&2
	exit 1
fi

annee=$1
categorie=$2

if ! echo $annee | grep -Exq '[0-9]+'; then
	echo "Erreur: l'année n'est pas un chiffre valide" >&2
	usage >&2
	exit 1
fi

if [ $annee -ge 2019 -o $annee -le 1901 ]; then
	echo "Erreur: l'année doit être comprise entre 1901 et 2018" >&2
	usage >&2
	exit 1
fi

json_file="$(mktemp)"

wget -o/dev/null -O "$json_file" "http://api.nobelprize.org/v1/prize.json"

jq ".prizes | .[] | select(.year==\"$annee\") | select(.category==\"$categorie\") | .laureates[] | \"\\(.firstname) \\(.surname)\"" \
	"$json_file" | sed 's/"//g'

rm -f "$json_file"
