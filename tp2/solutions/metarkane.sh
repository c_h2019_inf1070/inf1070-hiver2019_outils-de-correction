#!/bin/bash

usage()
{
  echo "$0 <word_src> <taille_partie_texte> <out_dir> <texte>..."
  echo "Éclate un texte en parties égales et le cache dans des fichiers générés à partir de 'word_src'"
  echo "Si la taille du texte n'est pas un multiple de <taille_partie_texte>, le dernier morceau sera complété par des espaces"
  echo "Les fichiers seront écrits dans le répertoire <out_dir>"
}

split_text() {
  l=$1
  shift
  fold -w $l <<< "$@";
}
words() { shuf -n$1 "$2" | sort ; }

if [[ $# < 4 ]]; then
  echo "Erreur: pas assez d'arguments" >&2
  usage >&2
  exit 1
fi

if ! [[ -f $1 ]]; then
  echo "Erreur: le fichier $1 n'existe pas" >&2
  usage >&2
  exit 1
fi

word_src="$1"

if ! echo "$2" | grep -Eq '[0-9]+'; then
  echo "Erreur: taille de chaque partie de texte invalide." >&2
  usage >&2
  exit 1
fi

partlen="$2"

outdir="$3"
mkdir -p "$outdir"

shift 3

while read -d ',' randword; do
  IFS= read linepart
  grep -ao '[a-zA-Z0-9]' < /dev/urandom | tr -d '\n' | head -c250 >"$outdir/$randword"
  echo -n "$linepart" >>"$outdir/$randword"
  [[ ${#linepart} < $partlen ]] && printf ' %.0s' $(seq $(($partlen-${#linepart}))) >>"$outdir/$randword"
done < <(paste -d',' <(words "$(split_text $partlen "$@" | wc -l)" "$word_src") <(split_text $partlen "$@"))

# vim: set sts=2 ts=2 sw=2 tw=120 et :

