#!/bin/bash

#test de la presence des arguments
if (( $# != 2 )); then
  echo "Minitac: Usage: ./minitac.sh fichier_source fichier_renverse" >&2
  exit 1
fi

if [[ -e "$1" ]]; then
  cat -n "$1" | sort -t ' ' -k1 -rn | gawk '{print $2}' >"$2"
else
  echo "Minitac: Le fichier source est introuvable" >&2
  exit 2
fi
