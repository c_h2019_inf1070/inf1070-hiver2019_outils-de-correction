#!/bin/sh

# Valeur par defaut
max=5
if [ -n "$1" ];
then
    # Check si l'argument est bien un nombre inferieur a 25, sinon msg d'erreur et sortie
    if [ "$1" != `echo "$1" | grep -oE "[0-9]+"` ] || [ "$1" -gt 25 ];
    then
	echo "Minitop: Entrez un parametre numerique inferieur ou egal a 25." >&2
	exit 1;
    else
	max="$1";
    fi
fi

# Calcule le nb de lignes provoquees par la commande ps
# pour que le nombre de ligne vers le haut ne depasse pas le nb de lignes total.

# Cas a priori rare et pas specifie dans l'enonce certains etudiants ont pose la question, je propose de compter ca comme bonus

maxPs=`ps -a | wc -l`;
if [ "$maxPs" -lt "$max" ];
then
    max=$maxPs
fi


# Cas special : si l'argument est negatif on ne voulait pas de boucle
if [ $max -le 0 ];
then
    ps -ax -o user,pid,pcpu,pmem,time,comm --sort time | tail -n 10
else
    while [ 1 ]
    do
	ps -ax -o user,pid,pcpu,pmem,time,comm --sort time | tail -n $max
	sleep 1;

	# Boucle des lignes qui remontent le curseur vers le haut
	tmp=0
	while [ $tmp -lt $max ]
	do
	    echo -n "\033[1A\033[K"
	    tmp=$(( $tmp+1 ))
	done;
    done;
fi
