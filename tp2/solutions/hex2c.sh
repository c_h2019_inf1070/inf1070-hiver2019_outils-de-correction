#!/bin/bash

usage()
{
	echo "hex2c [-h] <fichier>"
	echo "hex2c convertit une suite de nombre hexadécimaux en caractères."
	echo "Le fichier doit soit être un fichier existant, soit '-' pour lire depuis l'entrée standard"
	echo ""
	echo "OPTIONS:"
	echo "-h: mode de compatibilité hexdump. Lit une entrée sous le format généré par 'hexdump'"
}


if [ $# -eq 0 ]; then
	echo "Erreur: il manque un fichier en argument" >&2
	usage >&2
	exit 1
fi

opt="$1"

if [ $opt = "-h" ]; then
	hexdump_mode="Y"
	shift
fi

in_file="$1"
if [ "$in_file" != '-' -a ! -f "$in_file" ]; then
	echo "Erreur: fichier $in_file inexistant" >&2
	exit 1
fi

in_tmp="$(mktemp)"
cat "$in_file" | while read hexline; do
	if [ ! -z $hexdump_mode ]; then
		hexline="$(echo "$hexline" | \
			sed -E 's/^[a-fA-F0-9]+ ?(.*)$/\1/' | \
			sed -E 's/([0-9a-f]{2})([0-9a-f]{2})/\2 \1/g')"
	fi
	for hexc in $hexline; do
		printf "\x$(echo "$hexc")"
	done
done
