#!/bin/sh

trans_table()
{
	cat <<EOF
un 1
deux 2
trois 3
quatre 4
cinq 5
six 6
sept 7
huit 8
neuf 9
zero 0
plus +
moins -
fois *
divise \\\\/
modulo %
EOF
}

usage()
{
	echo "$0 <fichier_calc>"
	echo "Interprète et évalue un fichier avec des calculs"
}

if [ $# -ne 1 ]; then
	echo "Erreur: il manque le chemin vers le fichier d'entrée" >&2
	usage >&2
	exit 1
fi

if [ ! -f $1 ]; then
	echo "Erreur: le fichier $1 n'existe pas" >&2
	usage >&2
	exit 1
fi

out_file="$(mktemp)"
cp "$1" "$out_file"

trans_table | while read lettres chiffre; do
	sed -i "s/$lettres/$chiffre/g" "$out_file"
done

sed -Ei 's/([0-9]) /\1/g' "$out_file"

cat "$out_file" - | bc
