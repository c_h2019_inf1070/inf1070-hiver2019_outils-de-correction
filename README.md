
# INF1070 - Outils de correction - Session Automne 2019

Ce dépôt contient les outils de correction pour le cours INF1070 de l'Université
du Québec à Montréal.

Le présent dépôt contient les routines de correction du Travail Pratique 1 et 2
de la session automne 2019.

## Outils

Les outils de correction de ce dépôt sont des scripts assitant le correcteur
dans le parcours des différents numéros du travail pratique à corriger.

### TP1: Extension Vim

L'extension Vim `tp1/corriger.vim` permet la navigation et l'attribution de note
rapide dans le fichier Markdown pour le gabarit `tp1/évaluation.md`. Ci-après la
liste des différentes fonctionnalités:

* Création d'un fichier d'évaluation à partir du fichier de solution de
  l'étudiant.
* Identification automatique de l'étudiant à partir des information
  qu'il a lui même ajouté dans son fichier.
* Naviguer à travers les missions avec un simple série de touche (`[[`,
  `]]`).
* Attribution de note pour une mission en fonction de la position du
  curseur (`{note}gm` avec {note} étant la note).
* Navigation jusqu'à la section des commentaires d'une mission donnée
  (`gC`).
* Navigation jusqu'à la section des commentaires sur la présentation
  globale (`gp`)

**Utilisation**

Il suffit de lancer Vim sur le fichier du rapport de l'étudiant comme suit:

```sh
$ vim -S ${gabarit_evaluation} rapport_étudiant.md
```

### TP2: Script de test éclair

Les différents points suivants sont comptés parmis les fonctionnalités de
l'outil:

* Préparer le répertoire avec le fichier d'évaluation.
* Itérer à travers tous les exercices.
* Affichage coloré afin de faciliter la lecture de l'information.
* Démarrer la correction à un exercice donné (`-e`, voir `--help`).
* Des options pour réduire le nombre de questions (`-E`, `-C`).
* Gérer le signal d'interruption par l'utilisateur (`CTRL+C`). Le script écrit
  automatiquement la note dans le fichier d'évaluation.
* Affiche la différence entre la sortie du programme de l'étudiant et celle de
  la solution au format standard de *patch*.

**Utilisation**

Voir la [page de Wiki][wiki].

[wiki]: https://gitlab.com/c_h2019_inf1070/inf1070-hiver2019_outils-de-correction/wikis/Utilisation

#### Dépendances

* **`colordiff`**: Colore lune sortie de `diff` pour un meilleur affichage.
* **`xxd`**: Transformation de texte hexadécimal pour meilleure lecture.
* **`docker`** (optionnel): Exécution des scripts des étudiants dans un
    environnement `docker` (pour ne pas `rm -rf /` par inadvertance).
* **`tree`**: Affichage de répertoires de manière plus lisible.

## Méthodologie de contribution

Voir le fichier [`CONTRIBUER.md`](./CONTRIBUER.md).

## Auteur

Simon Désaulniers (<sim.desaulniers@gmail.com>)

<!-- vim: set sts=2 ts=2 sw=2 tw=80 et :-->

